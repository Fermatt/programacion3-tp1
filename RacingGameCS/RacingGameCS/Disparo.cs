﻿using System;
using System.Text;

namespace RacingGameCS
{
    class Disparo : Object
    {
        private bool disparada;
        private int contador;
        public Disparo(int _x, int _y) : base(_x, _y+1)
        {
            sprite = new string[2];
            sprite[0] = @"AAAADDDD";
            sprite[1] = @"AAAADDDD";
            disparada = false;
        }
        public void SetDisparada(bool shooting)
        {
            disparada = shooting;
        }
        public bool GetDisparada()
        {
            return disparada;
        }
        /*public void GetColision(Obstacle cosa)
        {
            if (cosa.getX() == x && cosa.getY() == y)
            {
                x = 0;
                y = 0;
                cosa.setX(Console.WindowWidth);
                cosa.setY(0);
                disparada = false;
            }
        }*/
        public override void update()
        {
            contador++;
            if (contador > 30)
            {
                if (x < Console.WindowWidth - sprite[0].Length && disparada == true)
                {
                    x++;
                    if (x >= Console.WindowWidth - sprite[0].Length)
                    {
                        disparada = false;
                        x = 0;
                        y = 1;
                    }
                }
                contador = 0;
            }
            draw();
        }
    }
}
