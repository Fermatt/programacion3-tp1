﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RacingGameCS
{
    [Serializable]
    abstract class Object
    {
        protected int x;
        protected int y;
        protected string[] sprite;

        protected Object(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public void setX(int _x)
        {
            x = _x;
        }

        public void setY(int _y)
        {
            y = _y;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public string[] getSprite()
        {
            return sprite;
        }

        public void setSprite(string[] _sprite)
        {
            sprite = _sprite;
        }

        protected void draw ()
        {
            for (int i = 0; i < sprite.Length; i++)
            {
                if ((x >= 0 && x < Console.WindowWidth) && (y + i >= 0 && y + i < Console.WindowHeight))
                {
                    Console.SetCursorPosition(x, y + i);
                    Console.WriteLine(sprite[i]);
                }
            }
        }

        public abstract void update();
    }
}
