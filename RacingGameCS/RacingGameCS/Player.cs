﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RacingGameCS
{
    [Serializable]
    class Player : Object
    {
        ConsoleKeyInfo keyPressed;
        private int _vidas;
        private int counter=0;

        public Player(int _x, int _y, int vidas):base(_x, _y)
        {
            sprite = new string[4];
            sprite[0] = @" _/\______\\__   ";
            sprite[1] = @"/ ,-. -|-  ,-.`-.";
            sprite[2] = @"`( o )----( o )-'";
            sprite[3] = @"  `-'      `-'   ";
            _vidas = vidas;

        }

        public void EXPLOTATODOVIEJA()
        {
            sprite[0] = @"   (_ ' ( `  )_  .__)   ";
            sprite[1] = @" ( (  (    )   `)  ) _) ";
            sprite[2] = @"(__ (_   (_ . _) _) ,__)";
            sprite[3] = @"    `~~`\ ' . /`~~`     ";
            draw();
        }

        public void SetVidas(int vidas)
        {
            _vidas = vidas;
        }

        public int GetVidas()
        {
            return _vidas;
        }
        public void Disparar(Disparo bala)
        {
            if (keyPressed.Key == ConsoleKey.Spacebar)
            {
                if (counter == 0)
                {
                    bala.SetDisparada(true);
                    bala.setX(x);
                    bala.setY(y);
                    counter++;
                }
            }
            else
            {
                counter = 0;
            }
        }
        public override void update()
        {
            if (Console.KeyAvailable)
            {
                keyPressed = Console.ReadKey(true);
                if (keyPressed.Key == ConsoleKey.Escape)
                {
                    FileStream fs = File.Create("posicion.dat");
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(fs, this);
                    fs.Close();
                    Environment.Exit(0);
                }
                if (keyPressed.Key == ConsoleKey.RightArrow)
                {
                    x++;
                    if (x > (Console.WindowWidth - sprite[0].Length))
                    {
                        x = Console.WindowWidth - sprite[0].Length;
                    }
                }
                else if (keyPressed.Key == ConsoleKey.LeftArrow)
                {
                    x--;
                    if (x < 0)
                    {
                        x = 0;
                    }
                }
                if (keyPressed.Key == ConsoleKey.DownArrow)
                {
                    y++;
                    if (y > (Console.WindowHeight - sprite.Length))
                    {
                        y = Console.WindowHeight - sprite.Length;
                    }
                }
                else if (keyPressed.Key == ConsoleKey.UpArrow)
                {
                    y--;
                    if (y < 0)
                    {
                        y = 0;
                    }
                }
            }
            draw();
            
        }
    }
}
