﻿using System;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Newtonsoft.Json.Linq;

namespace RacingGameCS
{
    static class Juego
    {
        static public void ejecutar()
        {
            loadWeather();
            loadName();
            FileStream fs;
            BinaryWriter bw;
            int highScore;
            int score = 0;
            if (!File.Exists("HighScore.dat"))
            {
                fs = File.Create("HighScore.dat");
                bw = new BinaryWriter(fs);
                highScore = 0;
                bw.Write(highScore);
                bw.Close();
                fs.Close();
            }
            else
            {
                BinaryReader br;
                fs = File.OpenRead("HighScore.dat");
                br = new BinaryReader(fs);
                highScore = br.ReadInt32();
                br.Close();
                fs.Close();
            }
            string[] title = new string[8];
            title[0] = @"__________               .__                   ________                       ";
            title[1] = @"\______   \_____    ____ |__| ____    ____    /  _____/_____    _____   ____  ";
            title[2] = @" |       _/\__  \ _/ ___\|  |/    \  / ___\  /   \  ___\__  \  /     \_/ __ \ ";
            title[3] = @" |    |   \ / __ \\  \___|  |   |  \/ /_/  > \    \_\  \/ __ \|  Y Y  \  ___/ ";
            title[4] = @" |____|_  /(____  /\___  >__|___|  /\___  /   \______  (____  /__|_|  /\___  >";
            title[5] = @"        \/      \/     \/        \//_____/           \/     \/      \/     \/ ";
            title[6] = "PRESS ENTER TO START";
            title[7] = "PRESS ESC TO EXIT";
            int x = Console.WindowWidth / 2 - title[0].Length / 2;
            int y = Console.WindowHeight / 2 - title.Length / 2;

            ConsoleKeyInfo keyPressed;
            bool escape = false;
            bool choque = false;
            int counter = 0;
            Console.CursorVisible = false;
            Player p;
            Obstacle dog;
            Obstacle car;
            Disparo disparito;
            while (!escape)
            {
                p = loadPosition();
                dog = new Obstacle(200, 1, "DOG");
                car = new Obstacle(400, 1, "CAR");
                disparito = new Disparo(0, 0);
                Console.Clear();
                Console.SetCursorPosition(0, 0);
                Console.Write("HighScore: " + highScore);
                for (int i = 0; i < title.Length; i++)
                {
                    if ((x >= 0 && x < Console.WindowWidth) && (y + i >= 0 && y + i < Console.WindowHeight))
                    {
                        Console.SetCursorPosition(x, y + i);
                        Console.WriteLine(title[i]);
                    }
                }
                keyPressed = Console.ReadKey(true);
                if (keyPressed.Key == ConsoleKey.Enter)
                {
                    score = 0;
                    while (p.GetVidas() > 0)
                    {
                        if (Collide(p, dog) && choque == false || Collide(p, car) && choque == false)
                        {
                            p.SetVidas(p.GetVidas() - 1);
                        }
                        if (Collide(p, dog))
                        {
                            choque = true;
                        }
                        else if (Collide(p, car))
                        {
                            choque = true;
                        }
                        else
                        {
                            choque = false;
                        }
                        if (Collide(disparito, dog) || Collide(disparito, car))
                        {
                            if (Collide(disparito, dog) && disparito.GetDisparada() == true)
                            {
                                dog.setX(0);
                                dog.setY(0);
                                disparito.SetDisparada(false);
                                disparito.setX(0);
                                disparito.setY(1);
                            }
                            if (Collide(disparito, car) && disparito.GetDisparada() == true)
                            {
                                car.setX(0);
                                car.setY(0);
                                disparito.SetDisparada(false);
                                disparito.setX(0);
                                disparito.setY(1);
                            }
                        }
                        dog.update();
                        car.update();
                        disparito.update();
                        p.update();
                        p.Disparar(disparito);
                        if (counter > 30)
                        {
                            Console.Clear();
                            counter = 0;
                            score++;
                            Console.SetCursorPosition(0, 0);
                            Console.Write(score);
                            Console.SetCursorPosition(10, 10);
                            Console.Write("Vidas: " + p.GetVidas());
                        }
                        counter++;
                    }
                    if (p.GetVidas() <= 0)
                    {
                        p.EXPLOTATODOVIEJA();
                    }
                    if(score > highScore)
                    {
                        highScore = score;
                    }
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine("GAME OVER");
                    Console.WriteLine("Press enter.");
                    Console.ReadLine();
                }
                else if (keyPressed.Key == ConsoleKey.Escape)
                {
                    escape = true;
                }
            }
            fs = File.OpenWrite("HighScore.dat");
            bw = new BinaryWriter(fs);
            bw.Write(highScore);
            bw.Close();
            fs.Close();
        }

        static bool Collide(Object a, Object b)
        {
           if (((a.getX() < b.getX() + b.getSprite()[0].Length) && (b.getX() < a.getX() + a.getSprite()[0].Length)) &&
              ((a.getY() < b.getY() + b.getSprite().Length) && (b.getY() < a.getY() + a.getSprite().Length)))
               {
                   return true;
               }
            return false;
        }

        static void loadWeather()
        {
            Console.Write("Escriba su ciudad: ");

            WebRequest req = WebRequest.Create("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+ Console.ReadLine() +"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

            WebResponse respuesta;
            
            try
            {
                respuesta = req.GetResponse();
            }

            catch (WebException)
            {
                Console.WriteLine("Error, no hay conexión");
                Console.ReadLine();
                Environment.Exit(0);
            }
            
            respuesta = req.GetResponse();

            Stream stream = respuesta.GetResponseStream();

            StreamReader sr = new StreamReader(stream);

            JObject data = JObject.Parse(sr.ReadToEnd());
            
            int error = (int)data["query"]["count"];

            if (error == 0)
            {
                Console.WriteLine("La ciudad no existe :(");
                Console.ReadLine();
            }
            else
            {
                string weather = (string)data["query"]["results"]["channel"]["item"]["condition"]["text"];

                if (weather == "Cloudy")
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                else if (weather == "Sunny")
                {
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                else if (weather == "Thunderstorms")
                {
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                }
            }
        }

        static void loadName()
        {
            if(!File.Exists("Name.txt"))
            {
                FileStream fs2 = File.Create("Name.txt");
                StreamWriter sw = new StreamWriter(fs2);
                Console.Write("Ingrese su nombre: ");
                string str = Console.ReadLine();
                sw.Write(str);
                sw.Close();
            }
            FileStream fs3 = File.OpenRead("Name.txt");
            StreamReader sr = new StreamReader(fs3);
            Console.Clear();
            Console.Write("Buen dia " + sr.ReadLine() + "!");
            Console.ReadLine();
        }
        static Player loadPosition()
        {
            FileStream fsP;
            if (!File.Exists("posicion.dat"))
            {
                //fsP = File.Create("position.dat");
                return new Player(15, 15, 3);
            }
            else
            {
                fsP = File.OpenRead("posicion.dat");
                BinaryFormatter formatter = new BinaryFormatter();
                Player p2 = (Player)formatter.Deserialize(fsP);
                fsP.Close();
                File.Delete("posicion.dat");
                return p2; // JAJAJAJAJAJAJAJAJAJAJAJJAAJJ
            }
        }
    }
}
