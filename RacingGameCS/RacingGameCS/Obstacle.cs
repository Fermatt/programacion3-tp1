﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RacingGameCS
{
    class Obstacle : Object
    {
        string type;
        int counter = 0;
        Random r;
        bool dir = false;
        public Obstacle(int _x, int _y, string _type):base(_x, _y)
        {
            type = _type;
            if (type == "DOG")
            {
                sprite = new string[5];
                sprite[0] = @"   __        ";
                sprite[1] = @"o-''|\_____/)";
                sprite[2] = @" \_/|_)     )";
                sprite[3] = @"     \  __  /";
                sprite[4] = @"     (_/ (_/ ";
            }
            else if(type == "CAR")
            {
                sprite = new string[5];
                sprite[0] = @"        _______      ";
                sprite[1] = @"       //  ||\ \     ";
                sprite[2] = @" .____//___||_\ \___ ";
                sprite[3] = @" |_/ \________/ \___|";
                sprite[4] = @"   \_/        \_/    ";

            }
            r = new Random();
        }

        public override void update()
        {
            counter++;
            if (counter > 30)
            {
                counter = 0;
                x--;
                if (x < 0 - sprite[0].Length)
                {
                    if (type == "CAR")
                        x = Console.WindowWidth;
                    else if (type == "DOG")
                        x = Console.WindowWidth;
                    y = r.Next(Console.WindowHeight-sprite.Length);
                }
                if (type == "CAR")
                {
                    if (dir)
                        y++;
                    else
                        y--;
                    if (y < 0 || y > Console.WindowHeight-sprite.Length)
                        dir = !dir;
                }
            }
            draw();
        }
    }
}
